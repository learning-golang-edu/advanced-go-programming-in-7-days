package main

import (
	"blockchain"
	"fmt"
)

func main() {
	bc := blockchain.NewBlockchain(blockchain.NewGenesisBlock())
	fmt.Println(bc.GetCurrentBlock().Hash)
	fmt.Println(blockchain.GetTransactionHash(*bc.GetCurrentBlock().Transaction))
	_ = bc.AddBlock(*blockchain.NewTransaction([]byte{4, 5, 6}))
	fmt.Println(bc.GetCurrentBlock().PreviousHash)
	fmt.Println(bc.GetCurrentBlock().Hash)
}
