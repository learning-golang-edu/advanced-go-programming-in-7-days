# Easy Issues

## Docker setup

Project have sample `docker-compose.yml` file from 
[confluentinc/cp-all-in-one-community](https://github.com/confluentinc/cp-all-in-one/tree/6.0.1-post/cp-all-in-one-community),
which includes full Kafka community edition from Confluent.

To start docker containers run following command:
```shell
docker-compose up -d
```

### Create Kafka topic
Application requires Kafka topic to send messages. To create one execute following command:
```shell
docker-compose exec broker kafka-topics --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic user-transactions
```

## Running
To start microservices execute following command in `easy-issues/cmd/auth` folder: 
```shell
go run main.go
```

then start user microservice from `easy-issues/cmd/users` folder:
```shell
go run app.go
```

To register new user execute following command:
```shell
curl -X POST 'http://localhost:8090/auth/register' -H "Authorization: Basic YWxhZGRpbkBtYWlsLmNvbTpvcGVuc2Vzc2FtZQ==" -vv
```

Users list can be seen at the following URL http://localhost:8091/api/users.