package application

import (
	"easy-issues/domain"
)

type IssueService struct {
	IssueRepository domain.IssueRepository
}

// Get an Issue by id
func (s IssueService) Issue(id int64) (*domain.Issue, error) {
	return s.IssueRepository.GetById(id)
}

// Returns all the Issues
func (s IssueService) Issues() ([]*domain.Issue, error) {
	return s.IssueRepository.All()
}

// Creates an Issue
func (s IssueService) Create(issue *domain.Issue) error {
	return s.IssueRepository.Create(issue)
}

// Deletes an Issue
func (s IssueService) Delete(id int64) error {
	return s.IssueRepository.Delete(id)
}
