package application

import (
	"easy-issues/domain"
)

type ProjectService struct {
	ProjectRepository domain.ProjectRepository
}

// Get a Project by id
func (s ProjectService) Project(id int64) (*domain.Project, error) {
	return s.ProjectRepository.GetById(id)
}

// Returns all the Projects
func (s ProjectService) Projects() ([]*domain.Project, error) {
	return s.ProjectRepository.All()
}

// Creates a Project
func (s ProjectService) Create(p *domain.Project) error {
	return s.ProjectRepository.Create(p)
}

// Deletes a Project
func (s ProjectService) Delete(id int64) error {
	return s.ProjectRepository.Delete(id)
}
