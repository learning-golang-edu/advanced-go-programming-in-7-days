package application

import (
	"easy-issues/domain"
	"encoding/json"
	"fmt"
	"github.com/Shopify/sarama"
	"reflect"
)

type UserService struct {
	UsersRepository domain.UserRepository
	EventsConsumer  sarama.Consumer
}

// Users returns all the Users
func (userService UserService) Users() ([]*domain.User, error) {
	return userService.UsersRepository.All()
}

// User gets a User by id
func (userService UserService) User(id int64) (*domain.User, error) {
	return userService.UsersRepository.GetById(id)
}

// Create creates a User
func (userService UserService) Create(u *domain.User) error {
	return userService.UsersRepository.Create(u)
}

// Delete deletes a User
func (userService UserService) Delete(id int64) error {
	return userService.UsersRepository.Delete(id)
}

func (userService UserService) OnEventTypeHandle(msgVal []byte, eventType interface{}) {
	var err error
	fmt.Println(reflect.ValueOf(eventType))
	switch eventType {
	case domain.CreateUserRegistrationEventType:
		event := new(domain.CreateUserRegistrationEvent)
		err = json.Unmarshal(msgVal, &event)
		if err != nil {
			fmt.Printf("Error parsing event %v", msgVal)
		}
		userService.UsersRepository.Create(
			&domain.User{
				Uuid:  event.EventId,
				Email: event.UserEmail,
			},
		)
	default:
		fmt.Println("Unknown Event type: ", eventType)
	}
}
