package application

import (
	json2 "encoding/json"
	"fmt"
	"github.com/Shopify/sarama"
	log2 "log"
)

var (
	Topic           = "user-transactions"
	Partition int32 = 0
	queues          = []string{"127.0.0.1:9092"}
	topics          = []string{Topic}
)

func newKafkaConfig() *sarama.Config {
	conf := sarama.NewConfig()
	conf.Producer.RequiredAcks = sarama.WaitForAll
	conf.Producer.Return.Successes = true
	conf.ChannelBufferSize = 1
	conf.Version = sarama.V0_10_2_1
	return conf
}

func NewKafkaSyncProducer() sarama.SyncProducer {
	kafka, err := sarama.NewSyncProducer(queues, newKafkaConfig())

	if err != nil {
		log2.Fatalf("Kafka error: %s\n", err)
	}

	return kafka
}

func NewKafkaConsumer() sarama.Consumer {
	consumer, err := sarama.NewConsumer(queues, newKafkaConfig())

	if err != nil {
		log2.Fatalf("Kafka error: %s\n", err)
	}

	return consumer
}

func SubmitEvent(kafka sarama.SyncProducer, event interface{}) error {
	json, err := json2.Marshal(event)
	if err != nil {
		return err
	}

	msLog := &sarama.ProducerMessage{
		Topic: Topic,
		Value: sarama.StringEncoder(json),
	}

	partition, offset, err := kafka.SendMessage(msLog)
	if err != nil {
		return err
	}

	fmt.Printf("Message is stored in partition %d, and offset %d\n", partition, offset)

	return nil
}

func ConsumeUserRegistrationEvents(consumer sarama.PartitionConsumer, handleEventType func(msgVal []byte, e interface{})) {
	var err error
	var msgItem []byte
	var log interface{}
	var logMap map[string]interface{}

	for {
		select {
		case err := <-consumer.Errors():
			fmt.Printf("Kafka error: %s\n", err)
		case msg := <-consumer.Messages():
			msgItem = msg.Value
			err = json2.Unmarshal(msgItem, &log)
			if err != nil {
				fmt.Printf("Parsing error: %s", err)
			}
			logMap = log.(map[string]interface{})
			logType := logMap["Type"]
			handleEventType(msgItem, logType)
		}
	}
}
