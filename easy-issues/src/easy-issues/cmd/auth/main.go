package main

import (
	easy_issues "easy-issues"
	"easy-issues/application"
	"easy-issues/domain"
	"easy-issues/persistence/db"
	"easy-issues/web"
	"easy-issues/web/handler"
	"log"
	"net/http"
	"time"
)

const serverAddr = "127.0.0.1:10000"

func main() {
	authRepo := db.NewAuthRepository()
	authService := application.AuthService{
		AuthRepository: authRepo,
	}
	authController := web.AuthController{
		AuthService:    authService,
		EventsProducer: application.NewKafkaSyncProducer(),
		Secret:         application.Secret,
	}

	//var opts []grpc.DialOption
	//opts = append(opts, grpc.WithInsecure())
	//conn, err := grpc.Dial(serverAddr, opts...)
	//if err != nil {
	//	log.Fatalf("fail to dial: %v", err)
	//}
	//defer conn.Close()
	//client := pb.NewUserClient(conn)
	//authController.UserClient = client

	pwhash, _ := easy_issues.HashPassword("opensessame")

	authRepo.Create(&domain.UserRegistration{
		Email:        "aladdin@mail.com",
		Uuid:         "1234",
		PasswordHash: pwhash,
	})

	mux := http.NewServeMux()
	mux.HandleFunc("/auth/login", authController.Login)
	mux.HandleFunc("/auth/register", authController.Register)
	mux.HandleFunc("/auth/verify", handler.JWTAuthHandler(authController.Verify))

	server := &http.Server{
		Addr:           ":8090",
		Handler:        mux,
		ReadTimeout:    5 * time.Second,
		WriteTimeout:   10 * time.Second,
		IdleTimeout:    120 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	log.Fatal(server.ListenAndServe())
}
