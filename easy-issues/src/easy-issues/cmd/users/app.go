package main

import (
	"easy-issues/application"
	"easy-issues/persistence/memory"
	"easy-issues/web/controller"
	"github.com/Shopify/sarama"
	_ "github.com/golang/protobuf/proto"
	_ "golang.org/x/net/http2"
	"log"
	"net/http"
	"time"
)

func main() {
	userRepo := memory.NewUserRepository()

	userService := application.UserService{
		UsersRepository: userRepo,
		EventsConsumer:  application.NewKafkaConsumer(),
	}

	userController := controller.UserController{
		UserService: userService,
	}

	//for i := 0; i < 10; i += 1 {
	//	userService.Create(&domain.User{Name: fmt.Sprintf("User_%d", i)})
	//}

	mux := http.NewServeMux()
	mux.HandleFunc("/health", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})
	mux.HandleFunc("/api/users", userController.List)

	// gRPC code
	//var opts []grpc.ServerOption
	//grpcServer := grpc.NewServer(opts...)
	//pb.RegisterUserServer(grpcServer, userController)
	//lis, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", 10000))
	//if err != nil {
	//	log.Fatalf("failed to listen: %v", err)
	//}
	//go func() {
	//	grpcServer.Serve(lis)
	//}()

	go func() {
		defer userService.EventsConsumer.Close()
		consumer, err :=
			userService.EventsConsumer.ConsumePartition(application.Topic, application.Partition, sarama.OffsetOldest)
		if err != nil {
			log.Fatalf("Kafka error: %s\n", err)
		}

		application.ConsumeUserRegistrationEvents(consumer, userService.OnEventTypeHandle)
	}()

	server := &http.Server{
		Addr:           ":8091",
		Handler:        mux,
		ReadTimeout:    5 * time.Second,
		WriteTimeout:   10 * time.Second,
		IdleTimeout:    120 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	log.Fatal(server.ListenAndServe())
}
