package web

import (
	//	"context"
	"easy-issues"
	"easy-issues/application"
	"easy-issues/domain"
	pb "easy-issues/protocol"
	"encoding/json"
	"github.com/Shopify/sarama"
	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"log"
	"net/http"
	"time"
)

// Controller for Issue model
type AuthController struct {
	AuthService    domain.AuthService
	EventsProducer sarama.SyncProducer
	Secret         string
	UserClient     pb.UserClient
}

type LoginResponse struct {
	Token string `json:"token"`
}

type JWTData struct {
	jwt.StandardClaims
	CustomClaims map[string]string `json:"custom,omitempty"`
}

func (c AuthController) Verify(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func (c AuthController) Login(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "Invalid method", http.StatusBadRequest)
	}

	email, password, ok := r.BasicAuth()
	if !ok {
		http.Error(w, "authorization failed", http.StatusUnauthorized)
	}

	userRegistration, err := c.AuthService.GetRegistrationByEmail(email)
	if err != nil {
		http.Error(w, "authorization failed", http.StatusUnauthorized)
	}

	if userRegistration.Status == domain.RegistrationStatusDeleted {
		http.Error(w, "authorization failed", http.StatusUnauthorized)
	}

	ok = easy_issues.CheckPasswordHash(password, userRegistration.PasswordHash)
	if !ok {
		http.Error(w, "authorization failed", http.StatusUnauthorized)
	}

	claims := JWTData{
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour).Unix(),
			Issuer:    "auth.service",
		},
		CustomClaims: map[string]string{
			"userId": userRegistration.Uuid,
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString([]byte(c.Secret))
	if err != nil {
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}

	response := LoginResponse{
		Token: tokenString,
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(response)
}

func (c AuthController) Register(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "invalid method", http.StatusBadRequest)
		return
	}

	// Code to Register new User account
	/*	This is a code for gRPC
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		newUserResponse, err := c.UserClient.SubmitNewUser(ctx, &pb.NewUserRequest{
			Email:  "alex@hotmail.com",
			Uuid:   "12345",
			Status: pb.UserStatus_ACTIVE,
		})
	*/
	event := domain.NewCreateUserRegistrationEvent(uuid.New().String(), "matti.testaaja@mail.com")
	err := application.SubmitEvent(c.EventsProducer, event)
	if err != nil {
		// log.Fatal(err)
		log.Fatalf("%v.SubmitEvent(_) = _, %v: ", c.EventsProducer, err)
	}

	//log.Println(newUserResponse)
}
