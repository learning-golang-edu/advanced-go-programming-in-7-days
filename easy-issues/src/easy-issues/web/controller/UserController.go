package controller

import (
	"context"
	"easy-issues/domain"
	pb "easy-issues/protocol"
	"encoding/json"
	"net/http"
)

// Controller for User model
type UserController struct {
	UserService domain.UserService
	pb.UnimplementedUserServer
}

func (c UserController) List(w http.ResponseWriter, r *http.Request) {
	users, err := c.UserService.Users()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	userJson, err := json.Marshal(users)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(userJson)
}

func (c UserController) SubmitNewUser(ctx context.Context, r *pb.NewUserRequest) (*pb.NewUserResponse, error) {
	// Create new user from NewUserRequest -> TODO
	return &pb.NewUserResponse{
		Email:  r.Email,
		Uuid:   r.Uuid,
		Status: r.Status,
	}, nil
}

func (c UserController) Create(w http.ResponseWriter, r *http.Request) {
}

func (c UserController) Show(w http.ResponseWriter, r *http.Request) {
}

func (c UserController) Delete(w http.ResponseWriter, r *http.Request) {
}
