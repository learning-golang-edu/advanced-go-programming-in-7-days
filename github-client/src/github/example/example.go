package main

import (
	"context"
	"fmt"
	"github"
	"os"
)

var apiToken = os.Getenv("GITHUB_API_TOKEN")

func main() {
	ctx := context.Background()
	c := github.NewClient(ctx, apiToken)
	repos, _, err := c.Repositories.List(ctx, "luhtonen")
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println()
	fmt.Println("##### Repositories #####")
	fmt.Println()

	for _, repo := range repos {
		fmt.Println(repo)
	}
	fmt.Println()
	fmt.Printf("Total repositories: %d\n", len(repos))
}
