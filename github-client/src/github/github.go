package github

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"golang.org/x/net/context/ctxhttp"
	"golang.org/x/oauth2"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

const (
	defaultBaseURL      = "https://api.github.com/"
	acceptVersionHeader = "application/vnd.github.v3+json" // https://developer.github.com/v3/#current-version
)

// A Client manages communication with the Github API.
type Client struct {
	client *http.Client

	baseURL *url.URL

	Repositories *RepositoriesService
}

func (c *Client) GetBaseURL() *url.URL {
	u := *c.baseURL
	return &u
}

func (c *Client) SetBaseURL(urlStr string) error {
	// Make sure the given URL end with a slash
	if !strings.HasSuffix(urlStr, "/") {
		urlStr += "/"
	}

	baseUrl, err := url.Parse(urlStr)
	if err != nil {
		return err
	}
	c.baseURL = baseUrl
	return nil
}

func (c *Client) NewRequest(method, urlStr string, body interface{}) (*http.Request, error) {
	rel, err := url.Parse(urlStr)
	if err != nil {
		return nil, err
	}

	u := c.baseURL.ResolveReference(rel)
	buf, err := c.encodeRequestBody(body)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(method, u.String(), buf)
	if err != nil {
		return nil, err
	}

	if body != nil {
		req.Header.Set("Content-Type", "application/json")
	}
	req.Header.Set("Accept", acceptVersionHeader)

	return req, nil
}

func (c *Client) encodeRequestBody(body interface{}) (io.ReadWriter, error) {
	if body != nil {
		buf := new(bytes.Buffer)
		enc := json.NewEncoder(buf)
		enc.SetEscapeHTML(false)
		err := enc.Encode(body)
		if err != nil {
			return nil, err
		} else {
			return buf, err
		}
	}
	return nil, nil
}

func (c *Client) Do(ctx context.Context, req *http.Request, v interface{}) (*http.Response, error) {
	resp, err := ctxhttp.Do(ctx, c.client, req)
	if err != nil {
		return nil, err
	}

	defer func() {
		rerr := resp.Body.Close()
		if rerr != nil {
			err = rerr
		}
	}()

	err = CheckResponse(resp)
	if err != nil {
		return nil, err
	}

	err = c.decodeResponseBody(resp.Body, v)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

func (c *Client) decodeResponseBody(body io.ReadCloser, v interface{}) error {
	if v != nil {
		var err error
		err = json.NewDecoder(body).Decode(v)
		return err
	}
	return nil
}

func CheckResponse(resp *http.Response) error {
	if resp.StatusCode < 300 {
		return nil
	}

	errorResponse := &ErrorResponse{Response: resp}
	data, err := ioutil.ReadAll(resp.Body)
	if err == nil && data == nil {
		json.Unmarshal(data, errorResponse)
	}
	return errorResponse
}

type ErrorResponse struct {
	Response *http.Response // HTTP response that caused this error
	Message  string         `json:"message"` // error message
}

func (r *ErrorResponse) Error() string {
	path, _ := url.QueryUnescape(r.Response.Request.URL.Opaque)
	return fmt.Sprintf("%v %v: %d %v",
		r.Response.Request.Method, path,
		r.Response.StatusCode, r.Message)
}

func NewClient(ctx context.Context, token string) *Client {
	ts := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: token},
	)

	tc := oauth2.NewClient(ctx, ts)

	client := newClient(tc)
	return client
}

func newClient(httpClient *http.Client) *Client {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}

	c := &Client{client: httpClient}
	c.SetBaseURL(defaultBaseURL)
	c.Repositories = &RepositoriesService{client: c}
	return c
}
