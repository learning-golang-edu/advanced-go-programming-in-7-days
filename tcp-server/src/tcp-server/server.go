package main

import (
	"fmt"
	"log"
	"net"
	"time"
)

const defaultHostPort = ":9000"

func main() {
	tcpAddr, err := net.ResolveTCPAddr("tcp", defaultHostPort)
	if err != nil {
		log.Fatal(err)
	}

	listener, err := net.ListenTCP("tcp", tcpAddr)
	if err != nil {
		log.Fatal(err)
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Fatal(err)
		}

		go func() {
			dateTime := time.Now().String()
			conn.Write([]byte(fmt.Sprintf("Time is: %q", dateTime)))
			defer conn.Close()
		}()
	}
}
