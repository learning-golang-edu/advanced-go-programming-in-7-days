package main

import (
	"fmt"
	"webex"
)

func main() {
	app := webex.New()
	app.Use(func(ctx *webex.Context) {
		ctx.AddHeader("X-Info", "Hello")
	})
	app.Get("/", func(ctx *webex.Context) {
		ctx.Send("Hello World")
	})
	app.Post("/add/user", func(ctx *webex.Context) {
		name, _ := ctx.Query("name")
		if name == "" {
			ctx.Send("What's your name again?")
		} else {
			ctx.Send(fmt.Sprintf("Got Username: %s", name))
		}
	})
	app.Run()
}
